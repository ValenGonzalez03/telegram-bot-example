Feature: Award
    As a cinephile
    I want to be able to consult the awards won by a film

Scenario: Found one movie with awards won
    When I send "/awards Titanic"
    Then it should respond "Won 11 Oscars. 126 wins & 83 nominations total"

Scenario: Didn't find any movie
    When I send "/awards Toy Story 8"
    Then it should respond "Not available"