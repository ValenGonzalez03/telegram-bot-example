class MovieApi
  def initialize(key = '2d536ec0')
    @url = 'https://www.omdbapi.com/'
    @key = key
  end

  def get_movie_response(movie_name)
    response = Faraday.get(@url, { t: movie_name, apikey: @key })
    if JSON.parse(response.body)['Response'] == 'False'
      nil
    else
      JSON.parse(response.body)['Awards']
    end
  end
end
